package pl.edu.pwsztar.domain.validators;

public class LastNameValidator {

    public static boolean isValid(final String lastName) {

        return lastName != null && !lastName.isEmpty();
    }

}
