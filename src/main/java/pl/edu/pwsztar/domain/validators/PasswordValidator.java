package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {

    public static boolean isValid(final String password) {

        final int PASSWORD_MIN_LENGTH = 4;

        if(password == null || password.length() < PASSWORD_MIN_LENGTH) {
            return false;
        }

        String regex = "^([0-9a-zA-Z])+([!@#$%^&*()_+=])";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);

        return matcher.matches();
    }

}
