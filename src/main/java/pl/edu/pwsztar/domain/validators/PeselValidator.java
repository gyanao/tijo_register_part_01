package pl.edu.pwsztar.domain.validators;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {



    public static boolean isValid(final String pesel) {
        int S = 0;

        if (pesel == null || pesel.length() != 11) return false;


        String regex = "^[0-9]+$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pesel);

        if (!matcher.matches()) return false;

        int[] arr = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

        String[] peselArr = pesel.split("");

        for (int i = 0; i < 10; i++) {
            System.out.println(S);
            S += Integer.parseInt(peselArr[i]) * arr[i];
        }

        S = S % 10;
        int ck = 10-S;
        return ((ck == Integer.parseInt(peselArr[10]) || (S == 0 && Integer.parseInt(peselArr[10]) == 0)));
    }


}
